# Generate divisions of number n in to k components.
def divide(n, k)
  division, decrease, increase = set(n, k, n - k + 1), 1, 2
  while division.sum == n
    if increase > k - 1 || decrease.zero?
      division, decrease, increase = set(n, k, division[0] - 1), 1, 2
    elsif division[decrease] - 1 >= division[increase] + 1
      division[decrease], division[increase] = division[decrease] - 1, division[increase] + 1
      p division.to_a
      decrease, increase = decrease + 1, increase + 1
    elsif division[decrease] - 1 < division[increase] + 1 && decrease.positive?
      decrease -= 1
      if increase <= k - 2 && decrease.zero?
        increase += 1
        decrease = increase - 1
      end
    else
      break
    end
  end
end

def set(n, k, l)
  division = [l]
  (1..k - 1).each do |i|
    sum = 0
    (0..i - 1).each { |j| sum += division[j] }
    division[i] = [n - sum - k + 1 + i, l].min
  end
  p division.to_a if division.sum == n
  division
end

if !ARGV[0].nil? && !ARGV[1].nil? && ARGV[0].to_i >= ARGV[1].to_i
  divide(ARGV[0].to_i, ARGV[1].to_i)
else
  p 'Please pass n and k in order, n must be bigger or equal than k'
end
