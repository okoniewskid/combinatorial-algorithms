# Generate permutations with minimal number of transpositions between them.
def b(m, i)
  if m.even? && m > 2
    if m - 1 > i then i
    else              m - 2
    end
  else m - 1
  end
end

def get_permutations_recursive(m)
  p $permutation if m == 1
  (1..m).each do |i|
    get_permutations(m - 1)
    $permutation[b(m, i) - 1], $permutation[m - 1] = $permutation[m - 1], $permutation[b(m, i) - 1] if i < m
  end
end

def get_permutations_non_recursive(n)
  visited, m, i, perm = Array.new(n + 1, 1), 1, 1, 1
  p "Permutation #{perm} = #{$permutation}"
  while i < n
    i = visited[m]
    visited[m] += 1
    visited[m]  = 1 if visited[m] > m
    if i == m
      m += 1
    else
      $permutation[b(m, i) - 1], $permutation[m - 1] = $permutation[m - 1], $permutation[b(m, i) - 1] if i < m
      perm, m = perm + 1, 1
      p "Permutation #{perm} = #{$permutation}"
    end
  end
end

if !ARGV[0].nil? && ARGV[0].to_i.positive?
  n, $permutation = ARGV[0].to_i, []
  (1..n).each { |i| $permutation[i - 1] = i }
  get_permutations_non_recursive(n)
else
  p 'Please pass array length when calling program'
end
