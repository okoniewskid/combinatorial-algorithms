# Searching pattern in text using a little better version of naive string matching algorithm.
def augumented_naive_string_matcher(string, pattern)
  index, skip = 0, 1
  while index <= string.length - pattern.length
    index = case string[index + skip - 1]
            when pattern[0] then index + check(string, pattern, index)
            else                 index + 1
            end
  end
end

def check(string, pattern, index)
  skip = 0
  (1..pattern.length - 1).each do |i|
    skip += 1
    break                                          if string[index + i] != pattern[i]
    p "Wzorzec wystepuje z przesunieciem #{index}" if i == pattern.length - 1
  end
  skip
end

augumented_naive_string_matcher(ARGV[0], ARGV[1])
