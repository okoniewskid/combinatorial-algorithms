# Generate subsets in lexicographic order.
class Node
  attr_accessor :father, :left_son, :right_son, :star, :n, :k, :stack
  def initialize(father, n, k, star, stack = ["\n"])
    @father    = father
    @left_son  = nil
    @right_son = nil
    @stack     = stack
    @star      = star
    @n         = n
    @k         = k
  end
end

def generate_subset(n, k)
  node = Node.new(nil, n, k, false)
  loop do
    p "node = G(#{node.n},#{node.k}), stack = #{node.stack.take(node.stack.length - 1)}"
    if node.n == node.k || node.k.zero?
      (1..node.k).each { |i| print i } if node.k.positive?
      node.stack.each  { |i| print i }
      break if node.father.nil?
      node = node.father
    elsif node.left_son.nil?
      case node.star
      when false then node = Node.new(node, node.n - 1, node.k, false, node.stack.dup)
      when true  then node = Node.new(node, node.n - 1, node.k - 1, false, node.stack.dup.unshift(node.n))
      end
      node.father.left_son = node
    elsif node.right_son.nil?
      case node.star
      when false then node = Node.new(node, node.n - 1, node.k - 1, true, node.stack.dup.unshift(node.n))
      when true  then node = Node.new(node, node.n - 1, node.k, true, node.stack.dup)
      end
      node.father.right_son = node
    else
      break if node.father.nil?
      node = node.father
    end
  end
end

if !ARGV[0].nil? && !ARGV[1].nil? && ARGV[0] >= ARGV[1]
  generate_subset(ARGV[0].to_i, ARGV[1].to_i)
else
  p 'Please pass n and k in order, n must be bigger or equal than k'
end
