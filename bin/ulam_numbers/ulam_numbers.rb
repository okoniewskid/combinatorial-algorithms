# Generate sequence of Ulam Numbers, assumption of task was to use 2 sieves.
def get_sieved_sequence(size, sequence = [1, 2])
  pairs = sequence.combination(2).to_a
  (3..size.to_i).each do |i|
    if sieve_uniq_sum?(i, pairs)
      sequence.push(i)
      pairs = sequence.combination(2).to_a
    end
  end
  sequence
end

def sieve_uniq_sum?(value, pairs)
  number_of_pairs = 0
  (0..pairs.length - 1).each do |i|
    number_of_pairs += 1 if pairs[i].sum == value
    break                if number_of_pairs > 1
  end
  case number_of_pairs
  when 1 then true
  else        false
  end
end

p get_sieved_sequence(ARGV[0])
