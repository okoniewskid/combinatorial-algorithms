# Generating permutations with minimal number of transpositions of adjacent elements.
def generate_permutations(n, k = 1)
  if n > 1
    permutations = generate_permutations(n - 1, k + 1)
    permutations_copy = permutations.dup
    permutations, j, go_right = [], 0, true
    while j < permutations_copy.length
      if go_right
        (0..n - 1).each { |i| create_permutation(i, j, k, permutations_copy, permutations) }
      else # go_left
        (n - 1).downto(0).each { |i| create_permutation(i, j, k, permutations_copy, permutations) }
      end
      j, go_right = j + 1, !go_right
    end
  else
    permutations = [[k]]
  end
  permutations
end

def create_permutation(i, j, k, permutations_copy, permutations)
  perm = permutations_copy[j].dup
  perm.insert(i, k)
  permutations << perm
end

if !ARGV[0].nil? && ARGV[0].to_i.positive?
  n = ARGV[0].to_i
  answer = generate_permutations(n)
  (1..answer.length).each { |i| puts "Permutation #{i} = [#{answer[i - 1]}]" }
else
  p 'Please pass array length when calling program'
end
